import matplotlib.pyplot as plt
import numpy as np

def imshow(tensor, title=None, plt_ax=plt, default=False):
    tensor = tensor.numpy().transpose((1, 2, 0))
    mean = np.array([0.485, 0.456, 0.406])
    std = np.array([0.229, 0.224, 0.225])
    tensor = tensor * std + mean
    tensor = np.clip(tensor, 0, 1)
    plt_ax.imshow(tensor)
    if title is not None:
        plt_ax.set_title(title)
    plt_ax.grid(False)


def show_label_samples(dataset, class_name):
    fig, axes = plt.subplots(nrows=3, ncols=2, figsize=(8, 10))
    fig.suptitle(class_name, fontsize=16)
    data = np.array(dataset.labels)
    indexes = np.where(data == class_name)[0]
    indexes = np.random.choice(indexes, size=6)
    for label_idx, ax in zip(indexes, axes.flatten()):
        im, label_num = dataset[label_idx]
        imshow(im.data, plt_ax=ax)


def show_loss(history):
    loss, acc, val_loss, val_acc = zip(*history)
    plt.figure(figsize=(15, 9))
    plt.plot(loss, label="train_loss")
    plt.plot(val_loss, label="val_loss")
    plt.legend(loc='best')
    plt.xlabel("epochs")
    plt.ylabel("loss")
    plt.show()
import os

import albumentations as A
import click
import cv2
from dotenv import find_dotenv, load_dotenv
import numpy as np
from pathlib import Path
from tqdm import tqdm


from preprocess_label import image_extensions



@click.command()
@click.argument('input_filepath', type=click.Path(exists=True))
@click.argument('output_filepath', type=click.Path(exists=True))
def augmentation_data(input_filepath, output_filepath):
    classes = ['city', 'field', 'forest', 'snow']
    transfom = A.Compose([
        A.RandomCrop(width=224, height=224, p=0.4),
        A.HorizontalFlip(p=0.7),
        A.VerticalFlip(p=0.7),
        A.RandomBrightnessContrast(p=0.3)
    ]
    )
    for class_name in classes:
        path = os.path.join(output_filepath, class_name)
        if not os.path.exists(path):
            os.mkdir(path)
        
        source_path = Path(os.path.join(input_filepath, class_name))
        path_generator = list(p.resolve() for p in source_path.glob("**/*") if str(p.suffix).lower() in image_extensions)
        for filepath in tqdm(path_generator):
            img = cv2.imread(str(filepath))
            img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
            transform_img = transfom(image=img)["image"]
            transform_img = cv2.cvtColor(transform_img, cv2.COLOR_RGB2BGR)
            img_name = os.path.basename(filepath)
            new_filepath = os.path.join(path, f"augment_{img_name}")
            cv2.imwrite(new_filepath, transform_img)


if __name__ == "__main__":
    project_dir = Path(__file__).resolve().parents[2]
    load_dotenv(find_dotenv())
    augmentation_data()

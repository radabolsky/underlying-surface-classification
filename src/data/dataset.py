import numpy as np
from pathlib import Path
from PIL import Image
from sklearn.preprocessing import LabelEncoder
from torch.utils.data import Dataset
from torchvision import transforms


class SurfaceDatset(Dataset):
    def __init__(self, files, mode, rescale_size=(224, 224)):
        super().__init__()
        self.files = sorted(files)
        self.mode = mode
        self.len_ = len(self.files)
        self.label_encoder = LabelEncoder()

        self.w = rescale_size[0]
        self.h = rescale_size[1]

        # if self.mode != "test":
        self.labels = [path.parent.name for path in self.files]
        self.label_encoder.fit(self.labels)

    def __len__(self):
        return self.len_
    
    def load_sample(self, file):
        image = Image.open(file)
        image.load()
        return image

    def _prepare_sample(self, image):
        image = image.resize((self.w, self.h))
        return np.array(image)
    
    def test_label(self, index):
        label = self.labels[index]
        label_id = self.label_encoder.transform([label])
        y = label_id.item()
        return y

    def __getitem__(self, index):
        transform = transforms.Compose([
            transforms.ToTensor(),
            transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
        ])
        x = self.load_sample(self.files[index])
        x = self._prepare_sample(x)
        x = np.array(x / 255, dtype=np.float32)
        x = transform(x)

        if self.mode == 'test':
            return x
        else:
            label = self.labels[index]
            label_id = self.label_encoder.transform([label])
            y = label_id.item()
            return x, y


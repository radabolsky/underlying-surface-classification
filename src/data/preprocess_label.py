# -*- coding: utf-8 -*-
import click
import logging
from pathlib import Path
from dotenv import find_dotenv, load_dotenv

import cv2
import numpy as np
import os
import shutil
import sys
import pandas as pd
from tqdm import tqdm
    

image_extensions = [
    ".jpg",
    ".jpeg",
    ".png",
    ".gif",
    ".bmp",
    ".tiff",
    ".ico",
    ".svg",
    ".webp",
    ".jfif",
    ".heif",
    ".bat",
    ".raw",
    ".exr",
    ".ppm",
    ".pgm",
    ".pbm",
    ".pnm",
    ".hdr",
    ".ai",
    ".eps"
]


def check_logger(path):
    if not os.path.exists(path):
        with open(path, 'w') as f:
            f.write("img_path,label")
            pass


class CreateLabelLoger:
    def __init__(self, labels, input_dir_path, output_dir_path):
        self.path = os.path.join(input_dir_path, 'label_logger.csv')
        print(self.path)
        check_logger(self.path)
        self.logger_df = pd.read_csv(self.path)
        self.output_dir_path = output_dir_path
        self.label_count = dict(zip(labels, [len(os.listdir(os.path.join(output_dir_path, label))) for label in labels]))
        self.keyboard_cuts = dict(zip([ord(str(num)) for num in range(len(labels))], labels))
    

    def add_label(self, img_path, label):
        row = {
            'img_path': [img_path],
            'label': [label]
        }
        self.logger_df = pd.concat([self.logger_df, pd.DataFrame(row)], ignore_index=True)
        self.label_count[label] = len(os.listdir(os.path.join(self.output_dir_path, label)))


    def back(self):
        self.logger_df.drop(index=self.logger_df.index[-1], axis=0, inplace=True)


    def save(self):
        self.logger_df.to_csv(self.path, index=False)


    def get_cur_pose(self):
        return self.logger_df.shape[0]
    



def check_dir(path):
    if not os.path.exists(path):
        os.mkdir(path)


@click.command()
@click.argument('input_filepath', type=click.Path(exists=True))
@click.argument('output_filepath', type=click.Path())
def main(input_filepath, output_filepath):
    """ Runs data processing scripts to turn raw data from (../raw) into
        cleaned data ready to be analyzed (saved in ../processed).
    """
    surface_classes = ['snow',
                       'city',
                       'field',
                       'forest']
    
    logger = CreateLabelLoger(labels=surface_classes,
                              input_dir_path=input_filepath,
                              output_dir_path=output_filepath)
    
    img_dir = sorted(os.listdir(input_filepath))
    n = len(img_dir)
    cur_pose = logger.get_cur_pose()
    for i in tqdm(range(cur_pose, n)):
        filename = img_dir[i]
        filepath = os.path.join(input_filepath, filename)
        name = ".".join(filename.split(".")[:-1])
        if filename.lower().endswith(tuple(image_extensions)):
            img = cv2.imread(filepath)
            org = [10, 25]
            for cut in sorted(logger.keyboard_cuts.keys()):
                label = logger.keyboard_cuts[cut]
                cv2.putText(img, f"{chr(cut)}: {label}({logger.label_count[label]})", org, cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 2, cv2.LINE_AA)
                org[1] += 25
            cv2.imshow('labeling', img)
            key = cv2.waitKey(0 )
            if key == ord('q'):
                logger.save()
                break
            if key == ord('a'):
                logger.back()
                logger.save()
            if key in logger.keyboard_cuts.keys():
                new_filepath = os.path.join(logger.output_dir_path, logger.keyboard_cuts[key], filename)
                logger.add_label(new_filepath, logger.keyboard_cuts[key])
                shutil.copy2(filepath,
                                new_filepath)
                logger.save()


if __name__ == '__main__':

    project_dir = Path(__file__).resolve().parents[2]

    load_dotenv(find_dotenv())

    main()




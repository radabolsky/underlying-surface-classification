from dataclasses import dataclass
import math

import cv2
import numpy as np


@dataclass 
class ImageSize:
    """Class for contain image size
    """
    w: int
    h: int


def xywh2xyxy(box):
    if len(box) == 4:
        return (box[0] - box[2] / 2,
                box[1] - box[3] / 2,
                box[0] + box[2] / 2,
                box[1] + box[3] / 2)
    else:
         return []
    

def xyxy2xywh(box):
    if len(box) == 4:
        return ((box[0] + box[2]) / 2,
                (box[1] + box[3]) / 2,
                box[2] - box[0],
                box[3] - box[1])
    else:
         return []
    

def normalize_box(box: list, img_size: ImageSize)->list:
    """Normalize box by image size

    Args:
        box (list): xyxy or xywh box
        img_size (ImageSize): image size

    Returns:
        list: normalized box
    """
    assert len(box) == 4

    return [box[0] / img_size.w,
            box[1] / img_size.h,
            box[2] / img_size.w,
            box[3] / img_size.h]


def convert_box_to_image_coords(xywhbox: list, image_size: ImageSize)->tuple:
    """Convert xywh normalized image format to xyxy coordinates of pixeles on image

    Args:
        xywhbox (list): normalized object box in xywh format
        image_size (ImageSize): image size

    Returns:
        tuple: pixels coordinates in xyxy format
    """
    box = xywh2xyxy(xywhbox)
    return tuple([box[0] * image_size.w,
                 box[1] * image_size.h,
                 box[2] * image_size.w,
                 box[3] * image_size.h])


def get_yolo_box(path: str) -> list:
    """Read label and return boxes

    Args:
        path (str): path to txt file in yolo format

    Returns:
        list: list of tuple boxes coordinates in normalized xywh format
    """
    box = []
    try:
        with open(path, 'r') as f:
            data = f.read()
            lines = data.split("\n")
            for line in lines:
                if len(line) == 0:
                    continue
                box.append(tuple(float(x) for x in line.split()[1::]))
    except:
        return []
    return box


def get_crossing_coords(box1: list, box2: list)->tuple:
    """Get coordinates of crossing boxes in xyxy format

    Args:
        box1 (list): first box coordinates in xyxy format
        box2 (list): second box coordinates in xyxy format

    Returns:
        tuple: tuple of np.array coordinates top left point and right bottom point
    """
    left_top = np.array([max(box1[0], box2[0]),
                         max(box1[1], box2[1])])
    right_bottom = np.array([min(box1[2], box2[2]),
                             min(box1[3], box2[3])])
    width, height = right_bottom - left_top
    if width <= 0 or height <= 0:
        return None, None
    else:
        return left_top, right_bottom


def is_boxes_crossing(box1: list, box2: list) -> bool:
    """Box crossing condition

    Args:
        box1 (list): first box coordinate in xyxy format
        box2 (list): second box coordinates in xyxy format

    Returns:
        bool: crossing box condition
    """
    left_top = np.array([max(box1[0], box2[0]),
                         max(box1[1], box2[1])])
    right_bottom = np.array([min(box1[2], box2[2]),
                             min(box1[3], box2[3])])
    width, height = right_bottom - left_top
    if width <= 0 or height <= 0:
        return False
    else:
        return True


class CropWindow:
    """Floating window for splitting high-resolution images
    """
    def __init__(self, image: cv2.Mat, normalized_boxes:list=None, crop_size: ImageSize=ImageSize(640, 640)):
        """Init floating window

        Args:
            image (cv2.Mat): original HR image
            normalized_boxes (list, optional): image labeling in normalized yolo format. Defaults to None.
            crop_size (ImageSize, optional): size of HR image. Defaults to ImageSize(640, 640).
        """
        self.__crop_size = crop_size
        self.__normalized_boxes = normalized_boxes
        h, w = image.shape[:2]
        self.__object_boxes = self.__get_image_boxes(ImageSize(w, h))
        self.__image = image
        self.__image_size = ImageSize(w=w, h=h)

    
    def __get_image_boxes(self, img_sz: ImageSize)->list:
        """Convert normalized HR image coordinates into xyxy pixels coordinates

        Args:
            img_sz (ImageSize): HR image size

        Returns:
            list: list of boxes in xyxy unnormalized format
        """
        crop_boxes = []
        if self.__normalized_boxes is None:
            return []
        for box in self.__normalized_boxes:
            crop_box = convert_box_to_image_coords(box, img_sz)
            crop_boxes.append(crop_box)
        return crop_boxes



    def create_random_sample(self):

        right_bottom = np.array([np.random.randint(self.__crop_size.w, self.__image_size.w),
                  np.random.randint(self.__crop_size.h, self.__image_size.h)])
        left_top = right_bottom - np.array([self.__crop_size.w, self.__crop_size.h])
        while self.check_crossing([left_top[0], left_top[1], right_bottom[0], right_bottom[1]]):
            right_bottom = np.array([np.random.randint(self.__crop_size.h, self.__image_size.h),
            np.random.randint(self.__crop_size.w, self.__image_size.h)])
            left_top = right_bottom - self.__crop_size
        return left_top, right_bottom
    

    def crop_random_sample(self):
        left_top, right_bottom = self.create_random_sample()
        return self.__image[left_top[1]: right_bottom[1],
                            left_top[0]: right_bottom[0]]
    

    def crop(self, box:list)->cv2.Mat:
        """Crop image by box

        Args:
            box (list): box in unnormalized xyxy format

        Returns:
            cv2.Mat: tile image
        """
        return self.__image[box[1]: box[3],
                            box[0]: box[2]]


    def crop_object(self):
        result = []
        crop_img = None
        for box in self.__object_boxes:
            crop_box = convert_box_to_image_coords(box, self.__image_size)
            crop_img = self.crop(crop_box)
            result.append(crop_img)
        return result
            

    def check_multiply_box(self, stride, label, w, h, left, top):
        for box in self.__object_boxes:
            if box[0] == left and box[1] == top:
                continue
            relate_box = np.array([coord - stride[i % 2] for i, coord in enumerate(box)]) 
            if relate_box[0] > 0 and relate_box[1] > 0 and relate_box[2] < w and relate_box[3] < h:
                xywh = xyxy2xywh(relate_box)
                label += f'\n0 {xywh[0] / w} {xywh[1] / h} {xywh[2] / w} {xywh[3] / h}'
        return label
    


    def tile_method(self):
        tiles = []
        ni = int(math.ceil(self.__image_size.h / self.__crop_size.h))  # up-down
        nj = int(math.ceil(self.__image_size.w / self.__crop_size.w))
        for i in range(ni):
            for j in range(nj):
                y2 = min((i + 1) * self.__crop_size.h, self.__image_size.h)
                y1 = max(y2 - self.__crop_size.h, 0)
                x2 = min((j + 1) * self.__crop_size.w, self.__image_size.w)
                x1 = max(x2 - self.__crop_size.w, 0)
                tile  = self.__image[y1:y2, x1:x2, :]
                tiles.append((x1, y1, x2, y2))
      
        obj_res = {
            'img': [],
            'labels': [],
            'tiles': tiles
        }

        falses_img = []
        for tile in tiles:
            img = self.__image[tile[1]:tile[3],
                               tile[0]:tile[2]]
            label = ''
            if img.shape[0] > 0 and img.shape[1] > 0:
                for box in self.__object_boxes:
                    top_left, bottom_right = get_crossing_coords(box, tile)
                    
                    if top_left is not None:
                        
                        xyxy = np.array([top_left[0], top_left[1], bottom_right[0], bottom_right[1]]) - np.array([tile[0], tile[1], tile[0], tile[1]])
                        xywh = xyxy2xywh(xyxy)
                        normalize_xywh = normalize_box(xywh, ImageSize(w=img.shape[1], h=img.shape[0]))
                        if len(label) > 0:
                            label += f"\n0 {' '.join([str(round(coord, 4)) for coord in normalize_xywh])}"
                        else:
                            label += f"0 {' '.join([str(round(coord, 4)) for coord in normalize_xywh])}"
                if len(label) > 0:
                    obj_res['img'].append(img)
                    obj_res['labels'].append(label)
                else:
                    falses_img.append(img)
        return obj_res, falses_img
    

    def random_method(self):
        obj_res = {
            'img': [],
            'labels': []
        }
        falses = self.crop_false(5)
        for crop in self.__object_boxes:
            center = np.array([crop[2] + (crop[2] - crop[0]) // 2, 
                               crop[3] + (crop[3] - crop[1]) // 2])
            
            
            rb_move = np.array([crop[0] + self.__crop_size.w // 2,
                                crop[1] + self.__crop_size.h // 2])
            
            lt_move = np.array([crop[2] - self.__crop_size.w // 2,
                                crop[3] - self.__crop_size.h // 2])
            
            if self.__image_size.h - center[1] < self.__crop_size.w // 2:
                rb_move[1] = self.__image_size.h
                lt_move[1] = self.__image_size.h - self.__crop_size.h

            if center[1] < self.__crop_size.h // 2:
                lt_move[1] = 0
                rb_move[1] = self.__crop_size.h - center[1]

            if self.__image_size.w - center[0] < self.__crop_size.w // 2:
                lt_move[0] -= self.__image_size[1] - self.__crop_size[0] + center[0]
                rb_move[0] = self.__image_size[1]
            
            if center[0] < self.__crop_size[0] // 2:
                lt_move[0] = 0
                rb_move[0] = self.__crop_size[0] - center[0]

            random_center = center
            
            lt_box = random_center - self.__crop_size // 2
            rb_box = random_center + self.__crop_size // 2

            box = (lt_box[0], lt_box[1], rb_box[0], rb_box[1])

            img = self.crop(box)
            h, w = img.shape[:2]

            if h > 0 and w > 0:
                xyxy = (crop[0] - lt_box[0],
                        crop[1] - lt_box[1],
                        crop[2] - lt_box[0],
                        crop[3] - lt_box[1])
                label = ''
                for obj_box in self.__object_boxes:
                    if is_boxes_crossing(obj_box, box):
                        top_left, bottom_right = get_crossing_coords(obj_box, box)
                        if top_left is not None:
                            xyxy = np.array([top_left[0], top_left[1], bottom_right[0], bottom_right[1]]) - np.array([box[0], box[1], box[0], box[1]])
                            xywh = xyxy2xywh(xyxy)
                            normalise_xywh = normalize_box(xywh, (w, h))
                            if len(label) > 0:
                                label += f"\n0 {' '.join([str(round(coord, 4)) for coord in normalise_xywh])}"
                            else:
                                label += f"0 {' '.join([str(round(coord, 4)) for coord in normalise_xywh])}"
                if len(label) > 0:
                    obj_res['img'].append(img)
                    obj_res['labels'].append(label)

        return obj_res, falses

    

    def check_crossing(self, potential_box):
        for box in self.__object_boxes:
            if is_boxes_crossing(potential_box, box):
                return True
        return False
    

    def crop_false(self, n=1):
        falses = []
        for i in range(n):
            image = self.crop_random_sample()
            falses.append(image)

        return falses


    def crop_object_random_window(self):
        croped = []
        labels = []

        for crop in self.__object_boxes:
            left = crop[0]
            right = crop[2]
            top = crop[1]
            bottom = crop[3]

            left_bound = right - self.__crop_size[0]
            right_bound = left + self.__crop_size[0]
            top_bound = bottom - self.__crop_size[1]
            bottom_bound = top + self.__crop_size[1]

            if left_bound <= 0:
                left_bound = 0
                right_bound = self.__crop_size[0]                    
            
            if right_bound >= self.__image_size[1]:
                right_bound = self.__image_size[1]
                left_bound = self.__image_size[1] - self.__crop_size[0]

            if top_bound < 0:
                top_bound = 0
                bottom_bound = self.__crop_size[1]

            if bottom_bound > self.__image_size[0]:
                bottom_bound = self.__image_size[0]
                top_bound = bottom_bound - self.__crop_size[1]

            center = np.array([np.random.randint(left_bound, right_bound),
                              np.random.randint(top_bound, bottom_bound)])

            top_left = center - self.__crop_size // 2
            bottom_right = center + self.__crop_size // 2
            if top_left[0] > left:
                top_left[0] = left
            if bottom_right[0] < right:
                bottom_right[0] = right
            if top_left[1] > top:
                top_left[1] = top
            if bottom_right[1] < bottom:
                bottom_right[1] = bottom
            box = (top_left[0], top_left[1], bottom_right[0], bottom_right[1])

            img = self.crop(box)

            xyxy = (crop[0] - top_left[0],
                    crop[1] - top_left[1],
                    crop[2] - top_left[0],
                    crop[3] - top_left[1])
            
            xywh = xyxy2xywh(xyxy)
            w, h = img.shape[1], img.shape[0]
            label = f'0 {xywh[0] / w} {xywh[1] / h} {xywh[2] / w} {xywh[3] / h}'
            label = self.check_multiply_box(self.__object_boxes, top_left, label, w, h, left, top)
            croped.append(img)
            labels.append(label)
        return croped, labels
    
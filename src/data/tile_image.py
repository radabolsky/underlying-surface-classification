import os

import click
import cv2
from pathlib import Path
from tqdm import tqdm


from crop import CropWindow

image_extensions = [
    ".jpg",
    ".jpeg",
    ".png",
    ".gif",
    ".bmp",
    ".tiff",
    ".ico",
    ".svg",
    ".webp",
    ".jfif",
    ".heif",
    ".bat",
    ".raw",
    ".exr",
    ".ppm",
    ".pgm",
    ".pbm",
    ".pnm",
    ".hdr",
    ".ai",
    ".eps"
]


@click.command
@click.argument('input_filepath', type=click.Path(exists=True))
@click.argument('output_filepath', type=click.Path())
def main(input_filepath, output_filepath):
    input_filepath = Path(input_filepath)
    output_filepath = Path(output_filepath)
    path_generator = list(p.resolve() for p in input_filepath.glob("**/*") if str(p.suffix).lower() in image_extensions)
    for filepath in tqdm(path_generator):
        img = cv2.imread(str(filepath))
        croper = CropWindow(img)
        for i in range(7):
            crop_img = croper.crop_random_sample()
            filename = os.path.basename(filepath)
            new_name = os.path.splitext(filename)[0] + f"_{i}" + os.path.splitext(filename)[1]
            cv2.imwrite(os.path.join(output_filepath, new_name), crop_img)

if __name__ == "__main__":
    main()
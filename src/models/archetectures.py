from torch import nn


class FirstArchitecture(nn.Module):
    def __init__(self, n_classes, device):
        super().__init__()
        self.device = device
        # Выход 3 х 224 х 224
        self.conv1 = nn.Sequential(
            nn.Conv2d(in_channels=3,
                      out_channels=8,
                      kernel_size=3),
            # выход 8 х 222 х 222
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=2),
            # выход 8 x 111 x 111
        )
        
        self.conv2 = nn.Sequential(
            nn.Conv2d(in_channels=8, out_channels=16, kernel_size=3),
            # выход 16 х 109 х 109
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=2)
            # выход 16 х 54 x 54
        )
        self.conv3 = nn.Sequential(
            nn.Conv2d(in_channels=16, out_channels=64, kernel_size=3),
            # выход 64 х 52 х 52
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=2)
            # выход 64 х 26 х 26
        )
        self.out = nn.Linear(64 * 26 * 26, n_classes)
        self.to(device)
        
    def forward(self, x):
        x = self.conv1(x)
        x = self.conv2(x)
        x = self.conv3(x)
        x = x.view(x.size(0), -1)
        x = self.out(x)
        return x
    

class AlexNet(nn.Module):
    def __init__(self, n_classes, device):
        super().__init__()
        self.device = device

        

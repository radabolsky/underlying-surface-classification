from pathlib import Path
import torch
from torch import nn
from torch.utils.data import Dataset, DataLoader
from tqdm import tqdm


def fit_epoch(model, train_loader, criterion, optimizer):
    running_loss = 0
    running_corrects = 0
    processed_data = 0

    for inputs, labels in train_loader:
        inputs = inputs.to(model.device)
        labels = labels.to(model.device)
        optimizer.zero_grad()

        outputs = model(inputs)
        loss = criterion(outputs, labels)
        loss.backward()
        optimizer.step()
        preds = torch.argmax(outputs, 1)
        running_loss += loss.item() * inputs.size(0)
        running_corrects += torch.sum(preds == labels.data)
        processed_data += inputs.size(0)

    train_loss = running_loss / processed_data
    train_acc = running_corrects.cpu().numpy() / processed_data
    return train_loss, train_acc


def eval_epoch(model: nn.Module, val_loader, criterion):
    model.eval()
    running_loss = 0
    running_corrects = 0
    processed_data = 0

    for inputs, labels in val_loader:
        inputs = inputs.to(model.device)
        labels = labels.to(model.device)
        with torch.set_grad_enabled(False):
            outputs = model(inputs)
            loss = criterion(outputs, labels)
            preds = torch.argmax(outputs, 1)
        running_loss += loss.item() * inputs.size(0)
        running_corrects += torch.sum(preds == labels.data)
        processed_data += inputs.size(0)
    
    val_loss = running_loss / processed_data
    val_acc = running_corrects.double() / processed_data
    return val_loss, val_acc


def train(train_dataset, val_dataset, model, epochs, optimizer, criterion, batch_size, tb_writer, model_dir=None):
    train_loader = DataLoader(dataset=train_dataset, batch_size=batch_size, shuffle=True)
    val_loader = DataLoader(dataset=val_dataset, batch_size=batch_size, shuffle=False)
    history = []
    log_template = "Epoch {ep:03d} train_loss: {t_loss:0.4f} val_loss {v_loss:0.4f} train_acc {t_acc:0.4f} val_acc {v_acc:0.4f}\n"
    best_val_loss = 1000000
    with tqdm(desc="train", total=epochs) as pbar_outer:
        for epoch in range(epochs):
            train_loss, train_acc = fit_epoch(model, train_loader, criterion, optimizer)
            val_loss, val_acc = eval_epoch(model, val_loader, criterion)
            history.append((train_loss, train_acc, val_loss, val_acc))
            if val_loss < best_val_loss:
                best_val_loss = val_loss
                if model_dir is not None:
                    model_path = Path(model_dir)
                    model_path = model_path / "best.pt"
                    torch.save(model.state_dict(), model_path)
            

            tb_writer.add_scalars('Training vs Validation Loss',
                                  {'Training': train_loss,
                                   'Validation': val_loss},
                                   epoch + 1)
            tb_writer.add_scalars("Training vs Validation Accuracy",
                                  {'Training': train_acc,
                                   'Validation': val_acc},
                                   epoch + 1)
            tb_writer.flush()

            pbar_outer.update(1)
            tqdm.write(log_template.format(ep=epoch+1,
                                           t_loss=train_loss,
                                           v_loss=val_loss,
                                           t_acc=train_acc,
                                           v_acc = val_acc))
    if model_dir is not None:
        model_path = Path(model_dir)
        model_path = model_path / 'last.pt'
        torch.save(model.state_dict(), model_path)
    return history